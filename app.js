const express = require('express')
const app = express()
const serveStatic = require('serve-static')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors()) // IF I USE THIS SHIT IN PROD, REMOVE THIS!
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(serveStatic('dist')) // Serves a static website

app.use(function (req, res, next) { // Does something (logs) every time a route is called
  console.log(req.path)
  next()
})

app.use('/api', require('./api/api'))
app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})

module.exports = app
