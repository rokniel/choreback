// request buckets from db
// buckets = response

const GetBuckets = function () {
  return new Promise((resolve, reject) => {
    let buckets = [
      {id: 1, name: 'Dessert', currentPoints: 3, totalPoints: 10},
      {id: 2, name: 'New toy', currentPoints: 35, totalPoints: 50}
    ]
    return resolve(buckets)
  })
}

module.exports = {
  GetBuckets: GetBuckets
}
