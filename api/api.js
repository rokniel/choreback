var express = require('express')
var router = express.Router()

// Buckets
let bucketApi = require('../lib/buckets')

router.get('/', function (req, res) {
  return new Promise((resolve, reject) => {
    bucketApi.GetBuckets()
      .then((result) => { res.send(result) })
  })
})

module.exports = router
